<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function role()
    {
        return $this->hasMany('App\Role', 'id', 'role_id');
    }

    public function hasRole($roles)
    {
        $this->haveRole = $this->getUserRole();

        // Check if the user is a root account
        if($this->haveRole->name == 'Root') {
            return true;
        }

        if(is_array($roles)){
            foreach($roles as $needRole){
                if($this->checkIfUserHasRole($needRole)) {
                    return true;
                }
            }
        } else{
            return $this->checkIfUserHasRole($roles);
        }

        return false;
    }

    private function getUserRole()
    {
        return $this->role()->getResults();
    }

    private function checkIfUserHasRole($needRole)
    {
        return (strtolower($needRole) == strtolower($this->haveRole->name)) ? true : false;
    }

    public function hasAccess(array $permissions) : bool
    {
        foreach ($this->role as $role) {
            if ($role->hasAccess($permissions)) {
                return true;
            }
        }

        return false;
    }
}
