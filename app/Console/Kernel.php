<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    protected $routeMiddleware = [
        'auth' 			=> 'App\Http\Middleware\Authenticate',
        'auth.basic' 	=> 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
        'guest' 		=> 'App\Http\Middleware\RedirectIfAuthenticated',
        'role' 		=> 'App\Http\Middleware\CheckRole',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
